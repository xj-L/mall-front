import {fetchOrders} from "../resource/fetch";
import {GET_ORDERS} from "../actionTypes/order";

export const getOrders = () => (dispatch) => {
  fetchOrders().then(response => {
    dispatch({
      type: GET_ORDERS,
      orders: response || []
    });
  });
};