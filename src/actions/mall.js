import {GET_GOODS} from "../actionTypes/mall";
import {fetchGoods} from "../resource/fetch";

export const getGoods = () => (dispatch) => {
  fetchGoods().then(response => {
    dispatch({
      type: GET_GOODS,
      goods: response || []
    });
  });
};