import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getOrders} from "../../actions/order";
import {deleteOrder} from "../../resource/fetch";
import './order.less';
import OrderTable from "../../components/orderTable/OrderTable";
import {NavLink} from "react-router-dom";

class Order extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.props.getOrders();
  }

  handleDelete(e) {
    deleteOrder(e.target.dataset.id).then(response => {
      if (response.status === 200) {
        console.log('delete success!');
        this.props.getOrders();
      } else {
        alert('删除失败，请稍后重试！');
      }
    });
  }

  render() {
    const { orders } = this.props;
    return <main id="order">
      {orders.length
        ? <OrderTable orders={orders} handleDelete={this.handleDelete}/>
        : <p className="tips">暂无订单，返回<NavLink to="/">商城页面</NavLink>继续购买！</p>}
    </main>;
  }
}

const mapStateToProps = state => {
  return {
    orders: state.order.orders || []
  }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);