import React, { Component } from 'react';
import './createGoods.less';
import {createGoods} from "../../resource/fetch";

class CreateGoods extends Component{
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      price: '',
      unit: '',
      url: ''
    };
    this.inputName = this.handleInput.bind(this, 'name');
    this.inputPrice = this.handleInput.bind(this, 'price');
    this.inputUnit = this.handleInput.bind(this, 'unit');
    this.inputURL = this.handleInput.bind(this, 'url');
    this.createGoods = this.createGoods.bind(this);
  }

  handleInput(type, e) {
    this.setState({
      [type]: e.target.value
    });
  }

  createGoods() {
    createGoods(this.state).then(response => {
      if (response.status === 201) {
        console.log('create goods success!');
        this.props.history.push('/');
      } else if (response.status === 409) {
        alert('商品名称已存在，请输入新的商品名称!');
      }
    });
  }

  render() {
    const { name, price, unit, url } = this.state;
    const isDisabled = !name || !price || !unit || !url;
    return <main id="create-goods">
      <h4>添加商品</h4>
      <form>
        <label htmlFor="name"><span className="required">*</span>名称：</label>
        <input type="text" id="name" onChange={this.inputName} value={this.state.name}/>
        <label htmlFor="price"><span className="required">*</span>价格：</label>
        <input type="number" id="price" onChange={this.inputPrice} value={this.state.price}/>
        <label htmlFor="unit"><span className="required">*</span>单位：</label>
        <input type="text" id="unit" onChange={this.inputUnit} value={this.state.unit}/>
        <label htmlFor="url"><span className="required">*</span>图片：</label>
        <input type="text" id="url" onChange={this.inputURL} value={this.state.url}/>
      </form>
      <button disabled={isDisabled} className="submit-btn" onClick={this.createGoods}>提交</button>
    </main>;
  }
}

export default CreateGoods;