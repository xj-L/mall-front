import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {getGoods} from "../../actions/mall";
import {connect} from "react-redux";
import GoodsItem from "../../components/goodsItem/GoodsItem";
import './mall.less';
import {createOrder} from "../../resource/fetch";

class Mall extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.props.getGoods();
  }

  handleClick(id) {
    createOrder({
      goodsId: id,
      count: 1
    }).then(response => {
      if (response.status === 201) {
        console.log('create success!');
      }
    })
  }

  render() {
    const goods = this.props.goods.map(g =>
      <GoodsItem
        id={g.id}
        name={g.name}
        price={g.price}
        unit={g.unit}
        url={g.url}
        key={g.name + g.price + g.unit}
        onHandleClick={this.handleClick}
      />);
    return <main id="mall">
      <ul>
        {goods}
      </ul>
    </main>;
  }
}

const mapStateToProps = state => {
  return {
    goods: state.mall.goods || []
  }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Mall);