import React, {Component} from 'react';
import './App.less';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Mall from './pages/mall/Mall';
import Order from './pages/order/Order';
import CreateGoods from './pages/createGoods/CreateGoods';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';

class App extends Component{
  render() {
    return (
      <div className='App'>
        <BrowserRouter>
          <Header/>
          <Switch>
            <Route path="/" exact component={Mall}/>
            <Route path="/order" component={Order}/>
            <Route path="/create-goods" component={CreateGoods}/>
            <Redirect to="/"/>
          </Switch>
          <Footer/>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;