import {combineReducers} from "redux";
import mall from './mall';
import order from './order';

export default combineReducers({
  mall,
  order
});