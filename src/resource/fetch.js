const BASE_GOODS_URL = 'http://localhost:8080/api/goods';
const BASE_ORDERS_URL = 'http://localhost:8080/api/orders';

export const fetchGoods = () => {
  return fetch(BASE_GOODS_URL,{
    method: 'GET'
  }).then(response => response.json());
};

export const createOrder = (data) => {
  return fetch(BASE_ORDERS_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
};

export const fetchOrders = () => {
  return fetch(BASE_ORDERS_URL,{
    method: 'GET'
  }).then(response => response.json());
};

export const deleteOrder = (id) => {
  return fetch(BASE_ORDERS_URL + '/' + id,{
    method: 'DELETE'
  });
};

export const createGoods = (data) => {
  return fetch(BASE_GOODS_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
};