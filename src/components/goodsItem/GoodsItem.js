import React from 'react';
import { MdAddCircleOutline } from "react-icons/md";
const defaultImage = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565086497652&di=c15f230ccaa4fa5e64698fb3f211a3db&imgtype=0&src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20170113%2F37b977b6ffd0467bb12d1b5ad1ec4998_th.jpeg";

const GoodsItem = ({ id, name, price, unit, url = defaultImage, onHandleClick }) => {
  return <li>
    <div className="img-wrapper">
      <img src={url || defaultImage} alt="oh no!"/>
    </div>
    <h5>{name}</h5>
    <p>单价：{price}元/{unit}</p>
    <MdAddCircleOutline className="add-btn" onClick={() => onHandleClick(id)}/>
  </li>;
};

export default GoodsItem;