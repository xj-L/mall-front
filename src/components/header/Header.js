import React from 'react';
import { MdHome } from "react-icons/md";
import { MdAddShoppingCart } from "react-icons/md";
import { MdAdd } from "react-icons/md";
import './header.less';
import {NavLink} from "react-router-dom";

const Header = () => {
  return <header>
    <nav>
      <ul>
        <NavLink exact to="/" className="link" activeClassName="active"><li><MdHome className="icon"/>商城</li></NavLink>
        <NavLink to="/order" className="link" activeClassName="active"><li><MdAddShoppingCart className="icon"/>订单</li></NavLink>
        <NavLink to="/create-goods" className="link" activeClassName="active"><li><MdAdd className="icon"/>添加商品</li></NavLink>
      </ul>
    </nav>
  </header>
};

export default Header;