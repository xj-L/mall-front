import React from 'react';

const OrderTable = ({orders, handleDelete}) => {
  const list = orders.map(order => {
    return <tr key={order.name + order.price + order.id}>
      <td>{order.name}</td>
      <td>{order.price}</td>
      <td>{order.count}</td>
      <td>{order.unit}</td>
      <td>
        <button data-id={order.id} className="delete-btn" onClick={handleDelete}>删除</button>
      </td>
    </tr>
  });
  return (
    <table className="order-table" cellSpacing="0">
      <thead>
      <tr>
        <th>名字</th>
        <th>单价</th>
        <th>数量</th>
        <th>单位</th>
        <th>操作</th>
      </tr>
      </thead>
      <tbody>
      {list}
      </tbody>
    </table>
  );
};

export default OrderTable;